/*
 * Copyright 2016 Arnaud Fonce <arnaud.fonce@r-w-x.net>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.rwx.netbeans.netkafa.server.nodes;

import java.awt.Image;
import javax.swing.Action;
import net.rwx.netbeans.netkafa.server.KarafInstanceImplementation;
import net.rwx.netbeans.netkafa.server.actions.DebugServerAction;
import net.rwx.netbeans.netkafa.server.actions.RemoveActionServer;
import net.rwx.netbeans.netkafa.server.actions.StartServerAction;
import net.rwx.netbeans.netkafa.server.actions.StopServerAction;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.util.ImageUtilities;
import org.openide.util.actions.SystemAction;
import org.openide.util.lookup.Lookups;

/**
 *
 * @author Arnaud Fonce <arnaud.fonce@r-w-x.net>
 */
public class KarafServerNode extends AbstractNode {

    private final KarafInstanceImplementation server;

    public KarafServerNode(KarafInstanceImplementation server) {
        super(Children.LEAF, Lookups.fixed(server));
        this.server = server;
        
    }

    @Override
    public String getDisplayName() {
        return server.getDisplayName();
    }

    @Override
    public Action[] getActions(boolean context) {
        return new Action[]{
            SystemAction.get(StartServerAction.class),
            SystemAction.get(DebugServerAction.class),
            SystemAction.get(StopServerAction.class),
            SystemAction.get(RemoveActionServer.class)
        //            SystemAction.get(StopServerAction.class),
        //            null,
        //            SystemAction.get(RemoveAction.class),
        //            null,
        //            SystemAction.get(ViewAdminConsoleAction.class),
        //            SystemAction.get(ViewServerLogAction.class)
        };
    }

    public KarafInstanceImplementation getServerimplementation() {
        return server;
    }

    @Override
    public Image getIcon(int type) {
        if( server != null && server.isRunning() ) {
            return ImageUtilities.loadImage("net/rwx/netbeans/netkafa/server/nodes/karaf-running-logo-16x16.png", true);
        }else {
            return ImageUtilities.loadImage("net/rwx/netbeans/netkafa/server/nodes/karaf-logo-16x16.png", true);
        }
    }
}
