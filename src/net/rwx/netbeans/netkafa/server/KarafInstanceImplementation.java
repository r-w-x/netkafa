/*
 * Copyright 2016 Arnaud Fonce <arnaud.fonce@r-w-x.net>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.rwx.netbeans.netkafa.server;

import net.rwx.netbeans.netkafa.server.nodes.KarafServerNode;
import java.io.File;
import java.util.concurrent.Future;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import net.rwx.netbeans.netkafa.Constants;
import net.rwx.netbeans.netkafa.KarafInstanceProvider;
import org.netbeans.api.debugger.jpda.DebuggerStartException;
import org.netbeans.api.debugger.jpda.JPDADebugger;
import org.netbeans.api.extexecution.ExecutionDescriptor;
import org.netbeans.api.extexecution.ExecutionService;
import org.netbeans.api.extexecution.ExternalProcessBuilder;
import org.netbeans.spi.server.ServerInstanceImplementation;
import org.netbeans.spi.server.ServerInstanceProvider;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.lookup.Lookups;

/**
 *
 * @author Arnaud Fonce <arnaud.fonce@r-w-x.net>
 */
public class KarafInstanceImplementation implements ServerInstanceImplementation {
    
    private final String instanceName;
    private final String karafHomePath;
    private final boolean cleanOnStart;
    private final Integer debuggerPort;
    
    private JPanel customizer;
    
    public KarafInstanceImplementation(
            String instanceName,
            String karafHomePath,
            boolean cleanOnStart,
            Integer debuggerPort) {
        this.instanceName = instanceName;
        this.karafHomePath = karafHomePath;
        this.cleanOnStart = cleanOnStart;
        this.debuggerPort = debuggerPort;
    }
    
    @Override
    public Node getFullNode() {
        return new KarafServerNode(this);
    }
    
    @Override
    public Node getBasicNode() {
        return new AbstractNode(Children.LEAF) {
            @Override
            public String getDisplayName() {
                return instanceName;
            }
        };
    }
    
    @Override
    public JComponent getCustomizer() {
        synchronized (this) {
            if (customizer == null) {
                customizer = new JPanel();
                customizer.add(new JLabel(instanceName));
            }
            return customizer;
        }
    }
    
    @Override
    public String getDisplayName() {
        return instanceName;
    }
    
    @Override
    public String getServerDisplayName() {
        return "Apache Karaf Server";
    }
    
    @Override
    public boolean isRemovable() {
        return !isRunning();
    }
    
    @Override
    public void remove() {
        KarafInstanceProvider provider = getServerInstanceProvider();
        provider.removeServer(instanceName);
    }
    
    private KarafInstanceProvider getServerInstanceProvider() {
        Lookup forPath = Lookups.forPath(Constants.INSTANCE_PROVIDER_PATH);
        return (KarafInstanceProvider) forPath.lookup(ServerInstanceProvider.class);
    }
    
    public String getExecutablePath() {
        String executableName = "karaf";
        if( isWindows() ) {
            executableName = "karaf.bat";
        }
        File executable = new File(new File(karafHomePath, "bin"), executableName);
        return executable.getAbsolutePath();
    }
    
    private boolean isWindows() {
        String osName = System.getProperty("os.name");
        return osName != null && osName.toLowerCase().contains("windows");
    }
    
    private Future<Integer> processFuture;
    
    public void start() {
        ExecutionDescriptor descriptor = new ExecutionDescriptor()
                .frontWindow(true)
                .controllable(true)
                .inputVisible(true)
                .preExecution(new Runnable() {
                    @Override
                    public void run() {
                        if (cleanOnStart) {
                            File data = new File(karafHomePath, "data");
                            if (data.exists() && data.isDirectory()) {
                                deleteDirectory(data);
                            }
                            File instances = new File(karafHomePath, "instances");
                            if (instances.exists() && instances.isDirectory()) {
                                deleteDirectory(instances);
                            }
                        }
                    }
                })
                .postExecution(new Runnable() {
                    @Override
                    public void run() {
                        getServerInstanceProvider().updateIcon();
                    }
                });
        
        ExternalProcessBuilder processBuilder
                = new ExternalProcessBuilder(getExecutablePath())
                .workingDirectory(new File(karafHomePath));
        
        ExecutionService service = ExecutionService.newService(
                processBuilder, descriptor, getDisplayName());
        
        processFuture = service.run();
        getServerInstanceProvider().updateIcon();
    }
    
    public static void deleteDirectory(File folder) {
        File[] files = folder.listFiles();
        if (files != null) {
            for (File f : files) {
                if (f.isDirectory()) {
                    deleteDirectory(f);
                } else {
                    f.delete();
                }
            }
        }
        folder.delete();
    }
    
    public boolean isRunning() {
        return processFuture != null && !processFuture.isDone();
    }
    
    private JPDADebugger debugger;
    
    public void debug() {
        try {
            if (!this.isRunning()) {
                this.start();
                Thread.sleep(500);
            }
            debugger = JPDADebugger.attach("localhost", debuggerPort, new Object[0]);
            debugger.waitRunning();
        } catch (DebuggerStartException | InterruptedException ex) {
            Exceptions.printStackTrace(ex);
        }
    }
    
    public boolean isDebuggerRunning() {
        return debugger != null
                && debugger.getState() != JPDADebugger.STATE_DISCONNECTED;
        
    }
    
    public void stop() {
        if (this.isRunning()) {
            processFuture.cancel(true);
        }
    }
}
