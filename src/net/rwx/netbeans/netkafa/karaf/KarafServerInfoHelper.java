/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.rwx.netbeans.netkafa.karaf;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import org.openide.util.Exceptions;

/**
 *
 * @author Arnaud Fonce <arnaud.fonce@r-w-x.net>
 */
public class KarafServerInfoHelper {

    public static String getServerInfoFromPath(String path) {
        File distributionInfo = new File(new File(path, "etc"), "distribution.info");
        File configProperties = new File(new File(path, "etc"), "config.properties");
        if (distributionInfo.exists() && distributionInfo.isFile()) {
            try (InputStream is = new FileInputStream(distributionInfo)) {
                Properties info = new Properties();
                info.load(is);
                return info.getProperty("name") + " " + info.getProperty("karafVersion");
            } catch (IOException ex) {
                Exceptions.printStackTrace(ex);
                return "";
            }
        } else if (configProperties.exists() && configProperties.isFile()) {
            try (InputStream is = new FileInputStream(configProperties)) {
                Properties info = new Properties();
                info.load(is);
                String systemPackages = info.getProperty("org.osgi.framework.system.packages");
                for (String systemPackage : systemPackages.split(",")) {
                    if (systemPackage.contains("org.apache.karaf.version")) {
                        return "Apache Karaf " + (systemPackage.split("="))[1].replace("\"", "");
                    }
                }
                return "";
            } catch (IOException ex) {
                Exceptions.printStackTrace(ex);
                return "";
            }
        } else {
            return "";
        }
    }
}
