/*
 * Copyright 2016 Arnaud Fonce <arnaud.fonce@r-w-x.net>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.rwx.netbeans.netkafa.wizard;

import java.io.IOException;
import java.util.Collections;
import java.util.Set;
import javax.swing.event.ChangeListener;
import net.rwx.netbeans.netkafa.Constants;
import net.rwx.netbeans.netkafa.KarafInstanceProvider;
import org.netbeans.spi.server.ServerInstanceProvider;
import org.openide.WizardDescriptor;
import org.openide.util.Lookup;
import org.openide.util.lookup.Lookups;

/**
 *
 * @author Arnaud Fonce <arnaud.fonce@r-w-x.net>
 */
public class KarafWizardIterator implements WizardDescriptor.InstantiatingIterator {

    private final KarafWizardPanel panel;
    private WizardDescriptor wizardDescriptor;
    
    public KarafWizardIterator() {
        panel = new KarafWizardPanel();
    }

    @Override
    public WizardDescriptor.Panel current() {
        return panel;
    }

    @Override
    public Set instantiate() throws IOException {
        if (wizardDescriptor.getValue() == WizardDescriptor.FINISH_OPTION) {
            KarafInstanceProvider provider = getServerInstanceProvider();
            provider.addNewServer(wizardDescriptor.getProperties());
        }
        return Collections.EMPTY_SET;
    }

    @Override
    public String name() {
        return "Karaf Instantiating Iterator";
    }

    @Override
    public boolean hasNext() {
        return false;
    }

    @Override
    public boolean hasPrevious() {
        return true;
    }

    @Override
    public void initialize(WizardDescriptor wizardDescriptor) {
        this.wizardDescriptor = wizardDescriptor;
    }

    @Override
    public void uninitialize(WizardDescriptor wd) {
    }

    @Override
    public void nextPanel() {
    }

    @Override
    public void previousPanel() {
    }

    @Override
    public void addChangeListener(ChangeListener cl) {
    }

    @Override
    public void removeChangeListener(ChangeListener cl) {
    }

    private KarafInstanceProvider getServerInstanceProvider() {
        Lookup forPath = Lookups.forPath(Constants.INSTANCE_PROVIDER_PATH);
        return (KarafInstanceProvider)forPath.lookup(ServerInstanceProvider.class);
    }
}
