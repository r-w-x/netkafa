/*
 * Copyright 2016 Arnaud Fonce <arnaud.fonce@r-w-x.net>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.rwx.netbeans.netkafa;

import net.rwx.netbeans.netkafa.wizard.KarafWizardIterator;
import org.netbeans.spi.server.ServerWizardProvider;
import org.openide.WizardDescriptor;
import org.openide.util.lookup.ServiceProvider;

/**
 *
 * @author Arnaud Fonce <arnaud.fonce@r-w-x.net>
 */
@ServiceProvider(service=ServerWizardProvider.class, path="Servers")
public class KarafWizardProvider implements ServerWizardProvider {

    @Override
    public String getDisplayName() {
        return "Apache Karaf Server";
    }

    @Override
    public WizardDescriptor.InstantiatingIterator getInstantiatingIterator() {
        return new KarafWizardIterator();
    }
    
}
